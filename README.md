## Windows Persistence Prep Project - Modular Implant

### Description

#### What this repo is
This repo aims to provide an outline for a single project which will satisfy all "demonstrate" requirements of the CCD Senior Windows Persistence JQR.

The idea is that developing against a project rather than individual requirements is more engaging and beneficial to learning (YMMV, of course).

This repository presents something of a requirements document which a developer can use to design and plan the implementation of an in-memory, modular RAT. Requirements will be mapped to specific JQR items, so that once a feature/requirement is implemented, a senior mentor can easily verify the requirement is met.

**Note:** The term *requirements* is used very lightly. There are no actual requirements to the project, merely a list of features which can be implemented to satisfy the JQR tasks. The entire project does not need to be completed to satisfy JQR tasks. Instead, you could implement individual modules to satisfy the tasks mapped to that module's functionality.

Since the project is tied to the WP JQR, it can of course be used to introduce or reinforce concepts documented there. This repo will not *teach* you those concepts, rather it serves as a starting point. Implementing those concepts is where you will learn those topics.

#### What this project is NOT
Small disclaimer, this project is not a project that will ensure success on the Windows Persistence exam. I am only certified in Windows Access, and I have not taken nor know anything about the Windows Persistence exam. It's very possible that concepts from this project will overlap (considering the concepts are taken from the JQR, it's not so farfetched), however there is no guarantee.

#### "Requirements"
##### General Project Requirements
**GPR1:** Project or project component can be compiled with CL and LINK via a script (batch, powershell, python, etc)<br>
**GPR2:** Project or project component can be compiled in Visual Studio<br>
**GPR3:** Project or project component is compiled as a .dll or .lib<br>
**GPR4:** Project or project component is statically linked<br>
**GPR5:** Project or project component uses a build event<br>
**GPR6:** Project or project component utilizes GetLastError/WSAGetLastError/NTSTATUS return values<br>
**GPR7:** Project or project component converts a win32 error code to a string via FormatMessage<br>
**GPR8:** At least one project component supports TCHAR via _tmain and associated T* macros<br>
**GPR9:** Project ensures no handles are leaked when exiting<br>
**GPR10:** Project or project component utilizes threading by using Win32 APIs<br>
**GPR11:** Project or project component utilizes threading by using POSIX APIs (_beginthread / _endthread, etc)

##### Error Library Requirements
**ELR1:** Remote components (core and modules) contain no strings and utilize only integers for reporting errors to local<br>
**ELR2:** Local components translate error codes from integers to strings via resource dlls<br>
**ELR3:** Resource dlls are loaded dynamically when a module is loaded<br>
**ELR4:** Error strings can be translated from integers into both multi-byte and wide character strings<br>
**ELR5:** Error strings are stored as multi-byte strings and are translated to wide character strings dynamically

##### Serialization Library Requirements
**SLR1:** Project supports data serialization via a shared library (.lib)<br>
**SLR2:** Serialization library serializes data using XML via IXMLDOMDocument COM objects

##### Local Component Requirements
**LCR1:** Local component can commnicate with remote component<br>
**LCR2:** Uses embedded private key to establish secure connection with local component<br>
**LCR3:** Generates a symmetric session key for use by the remote component

##### Remote Component Requirements
**RCR1:** Remote component can communicate with local component<br>
**RCR2:** Uses embedded public key to establish secure connection with local component<br>
**RCR3:** Uses a symmetric session key generated during handshake with local component to encrypt all communications<br>
**RCR4:** Contains only enough embedded functionality to establish a session with local component and load modules<br>
**RCR5:** All additional functionality is dynamically, in-memory loaded via modules<br>
**RCR6:** Modules are verified for integrity using a signed hash from local component<br>
**RCR7:** Remote component ensures only one instances is running by using a mutex<br>
**RCR8:** Modules operate in their own threads<br>
**RCR9:** Modules execute commands and return results in an asynchronous manner<br>
**RCR10:** Modules wait on a task list structure until the main component posts a tasks<br>
**RCR11:** Modules execute the task and post the results to a shared result structure 

##### Tunnel Module Requirements
**TMR1:** Project supports tunneling functionality as a module<br>
**TMR2:** Tunnel module supports tunneling both UDP and TCP protocols<br>
**TMR3:** Tunnel module supports an arbitrary number of tunnels<br>
**TMR4:** Tunnel module utilizes I/O completion ports

##### Interactive Shell Module Requirements
**ISMR1:** Project supports interactive shell (cmd, powershell) functionality as a module<br>
**ISMR2:** IS module supports running the shell as another user<br>
**ISMR3:** IS module communicates with child process using pipes<br>
**ISMR4:** IS module ensures all process handles/resources are released as necessary

##### Survey Module Requirements
**SMR1:** Project supports surveying of remote system as a module<br>
**SMR2:** Survey module enumerates directories and files<br>
**SMR3:** Survey module enumerates registry hives, keys, and values

##### Migration Module Requirements
**MMR1:** Project supports migration functionality as a module<br>
**MMR2:** Migration module supports enabling of necessary token privileges<br>
**MMR3:** Migration module supports migrating via simple dll injection

##### Persistence Module Requirements
**PMR1:** Project supports persistence functionality as a module<br>
**PMR2:** Persistence module utilizes the registry to regain execution on system restart<br>
**PMR3:** Persistence module stores its payload in registry as binary data

##### Files Module Requirements
**FMR1:** Project supports file I/O functionality as a module<br>
**FMR2:** Files module supports upload of files<br>
**FMR3:** Files module supports download of files<br>
**FMR4:** Files module uses asynchronous operations

##### Token Service Module Requirements
**TSMR1:** Project supports service that serves SYSTEM tokens to requesting process via as a module<br>
**TSMR2:** TS module supports creating and deleting a service<br>
**TSMR3:** TS module supports processes requesting a token<br>
**TSMR4:** TS module supports duplicating and serving a SYSTEM token<br>
**TSMR5:** TS module uses named pipes to serve tokens

##### Common Language Runtime (CLR) Module Requirements
**CMR1:** Project supports loading .NET assemblies as a module<br>
**CMR2:** CLR module loads the CLR into the unmanaged local component<br>
**CMR3:** CLR module supports loading .NET assemblies as modules

#### JQR Concepts Map
As of Cyber Capability Developer Windows Persistence JQR v1.2 (07 Apr 22)

| JQR Item | JQR Tasks | Project Requirement | 
|----------|:----------|:--------------------|
| **3.1.2**<br>Build Environment |Building C or C++ projects with cl and link<br>Building C or C++ projects in Visual Studio<br>Modify CL and LINK options in Visual Studio projects<br>Create and use a Visual Studio build event|[GPR1](#general-project-requirements)<br>[GPR2](#general-project-requirements),[SLR1](#serialization-library-requirements)<br>[GPR3](#general-project-requirements),[GPR4](#general-project-requirements)<br>[GPR5](#general-project-requirements)|
| **3.2.2**<br>Windows Foundations |Handling errors returned from Windows API calls<br>Retrieving and displaying system error messages<br>Usage of standard string types: CHAR, WCHAR, TCHAR, UNICODE_STRING, BSTR<br>Converting between CHAR and WCHAR strings| [GPR6](#general-project-requirements)<br>[GPR7](#general-project-requirements)<br>[GPR8](#general-project-requirements),[ELR1](#error-library-requirements),[ELR2](#error-library-requirements),[ELR3](#error-library-requirements),[ISMR2](#interactive-shell-module-requirements),[SLR2](#serialization-library-requirements)<br>[ELR4](#error-library-requirements),[ELR5](#error-library-requirements) |
| **3.3.2**<br>Objects and Handles |Proper management of handles<br>Sharing handles between processes as standard handles<br>Sharing handles between processes<br>Proper usage of Pseudo handles| [ISMR1](#interactive-shell-module-requirements),[ISMR4](#interactive-shell-module-requirements),[MMR1](#migration-module-requirements),[FMR1](#files-module-requirements),[TSMR1](#token-service-module-requirements)<br>[ISMR1](#interactive-shell-module-requirements),[ISMR2](#interactive-shell-module-requirements)<br>[TSMR3](#token-service-module-requirements),[TSMR4](#token-service-module-requirements)<br>[TSMR1](#token-service-module-requirements) |
| **3.4.2**<br>Processes |Creating processes<br>Creating a process using a token for a user other than the current user<br>Creating processes while passing standard handles<br>Management of process handles| Project mapping<br>Project mapping<br>Project mapping<br>Project mapping |
| **3.5.2**<br>Threads |Creating and managing threads<br>Proper threading when using the CRT| Project mapping<br>Project mapping |
| **3.6.2**<br>Synchronization Objects |Using the wait APIs<br>Using event objects<br>Using mutex objects<br>Using critical section objects| Project mapping<br>Project mapping<br>Project mapping<br>Project mapping |
| **3.7.2**<br>File and Device I/O |Creating new files<br>Safely reading and writing files<br>Opening windows devices<br>Reading and writing devices directly<br>Reading and writing files using overlapped I/O<br>Enumerating files and directories using Win32 API| Project mapping<br>Project mapping<br>Project mapping<br>Project mapping<br>Project mapping<br>Project mapping |
| **3.8.2**<br>Networking |Using Winsock2 APIs<br>Using I/O completion ports with sockets| Project mapping<br>Project mapping |
| **3.9.2**<br>Memory Management |Using the Virtual* allocation APIs<br>Using the Heap* allocation APIs<br>Modifying protections of allocated memory| Project mapping<br>Project mapping<br>Project mapping |
| **3.10.2**<br>Libraries (DLL / Static) |Using a DLL with implicit loading<br>Using a DLL with explicit loading<br>Using static libraries<br>Simple DLL injection<br>In-memory loading DLLs| Project mapping<br>Project mapping<br>Project mapping<br>Project mapping<br>Project mapping |
| **3.11.2**<br>Security |Enabling and disabling token privileges<br>Creating and using impersonation tokens| Project mapping<br>Project mapping |
| **3.12.2**<br>Registry |Creating registry keys and values<br>Enumerating registry keys and values<br>Persisting a program using a run key<br>Storing binary data in the registry| Project mapping<br>Project mapping<br>Project mapping<br>Project mapping |
| **3.13.2**<br>Interprocess Communication (IPC) Mechanisms |Using named pipes to communicate between processes| Project mapping |
| **3.14.2**<br>Services |Writing a simple windows service application<br>Managing service status<br>Creating and controlling services using Win32 API| Project mapping<br>Project mapping<br>Project mapping |
| **3.15.2**<br>Component Object Model (COM) |Properly initializing and cleaning up COM objects<br>Using simple COM interfaces| Project mapping<br>Project mapping |
| **3.16.2**<br>Crypto Next Generation (CNG) / Bcrypt |Encrypt and decrypt data using a symmetric algorithm using CNG<br>Encrypt and decrypt data using an asymmetric algorithm using CNG<br>Hash data using CNG<br>Generate random data using CNG| Project mapping<br>Project mapping<br>Project mapping<br>Project mapping |
| **3.17.2**<br>.NET and .NET Framework |Writing a simple .NET console application<br>In-memory loading a .NET assembly from a .NET application| Project mapping<br>Project mapping |
| **3.18.2**<br>Debugging |Setting and using a Windows debugger<br>Sending debug output to the debugger / system debugger| Project mapping<br>Project mapping |

#### Project Function Map
This table lays out a mapping of Win32 APIs/functions to the JQR concepts. It should help you ensure you have completed a JQR task, because once you have used those APIs/functions in your project you can link to it in your own README (once the below table is complete, so should be your JQR). And as long as your mentor agrees the API/function satisfies the JQR task, it should make their job of verifying that you have completed the task very simple.

**There are many ways to demonstrate proficiency for the different JQR tasks. The table below is merely a suggesstion and can be modified however you want/see fit.** I would only suggest that you change the table if using a different API/function, so that your mentor can still use the table for easily verifiying you have completed the task.

| JQR Item | Associated API/Function/Concept | Link to Source |
|----------|--------------------------------|----------------|
| **3.1.2 Build Environment** |||
|Building C or C++ projects with cl and link|[CL Batch Script](https://docs.microsoft.com/en-us/cpp/build/reference/compiling-a-c-cpp-program?view=msvc-170)| |
|Building C or C++ projects with cl and link|[LINK Batch Script](https://docs.microsoft.com/en-us/cpp/build/reference/linking?view=msvc-170)| |
|Building C or C++ projects in Visual Studio|[Compile and Build](https://docs.microsoft.com/en-us/visualstudio/ide/compiling-and-building-in-visual-studio?view=vs-2022)| |
|Modify CL and LINK options in Visual Studio projects|[Build a DLL](https://docs.microsoft.com/en-us/cpp/build/reference/dll-build-a-dll?view=msvc-170)| |
|Modify CL and LINK options in Visual Studio projects|[Statically Link](https://docs.microsoft.com/en-us/cpp/build/reference/md-mt-ld-use-run-time-library?view=msvc-170)| |
|Create and use a Visual Studio build event|[Specifying Build Events](https://docs.microsoft.com/en-us/cpp/build/specifying-build-events?view=msvc-170)| |
|Create and use a Visual Studio build event|[Custom Build Steps](https://docs.microsoft.com/en-us/cpp/build/understanding-custom-build-steps-and-build-events?view=msvc-170)| |
| **3.2.2 Windows Foundations**| | |
|Handling errors returned from Windows API calls|[GetLastError](https://docs.microsoft.com/en-us/windows/win32/api/errhandlingapi/nf-errhandlingapi-getlasterror)| |
|Handling errors returned from Windows API calls|[SetLastError](https://docs.microsoft.com/en-us/windows/win32/api/errhandlingapi/nf-errhandlingapi-setlasterror)| |
|Handling errors returned from Windows API calls|[WSAGetLastError](https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-wsagetlasterror)| |
|Handling errors returned from Windows API calls|[WSASetLastError](https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-wsasetlasterror)| |
|Handling errors returned from Windows API calls|[SUCCEEDED](https://docs.microsoft.com/en-us/windows/win32/api/winerror/nf-winerror-succeeded)| |
|Handling errors returned from Windows API calls|BCRYPT_SUCCESS (bcrypt.h)| |
|Retrieving and displaying system error messages|[FormatMessage](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-formatmessage)| |
|Usage of standard string types: CHAR, WCHAR, TCHAR, UNICODE_STRING, BSTR|[main/wmain/_tmain](https://docs.microsoft.com/en-us/cpp/cpp/main-function-command-line-args?view=msvc-170)| |
|Usage of standard string types: CHAR, WCHAR, TCHAR, UNICODE_STRING, BSTR|[LsaLookupNames2 (UNICODE_STRING)](https://docs.microsoft.com/en-us/windows/win32/api/ntsecapi/nf-ntsecapi-lsalookupnames2)| |
|Usage of standard string types: CHAR, WCHAR, TCHAR, UNICODE_STRING, BSTR|[LsaLookupSids2 (UNICODE_STRING)](https://docs.microsoft.com/en-us/windows/win32/api/ntsecapi/nf-ntsecapi-lsalookupsids2)| |
|Usage of standard string types: CHAR, WCHAR, TCHAR, UNICODE_STRING, BSTR|[XML DOM (BSTR)](https://docs.microsoft.com/en-us/previous-versions/windows/desktop/ms760218(v=vs.85))| |
|Usage of standard string types: CHAR, WCHAR, TCHAR, UNICODE_STRING, BSTR|[Task Scheduler 2.0 (BSTR)](https://docs.microsoft.com/en-us/windows/win32/taskschd/task-scheduler-2-0-interfaces)| |
|Converting between CHAR and WCHAR strings|[MultiByteToWideChar](https://docs.microsoft.com/en-us/windows/win32/api/stringapiset/nf-stringapiset-multibytetowidechar)| |
|Converting between CHAR and WCHAR strings|[WideCharToMultiByte](https://docs.microsoft.com/en-us/windows/win32/api/stringapiset/nf-stringapiset-widechartomultibyte)| |
|Converting between CHAR and WCHAR strings|[CString](https://docs.microsoft.com/en-us/cpp/atl-mfc-shared/using-cstring?view=msvc-170)| |
| **3.3.2 Objects and Handles** | |
|Proper management of handles|[CloseHandle](https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle)| |
|Sharing handles between processes as standard handles|[CreateProcess](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createprocessw)| |
|Sharing handles between processes as standard handles|[STARTUPINFO](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/ns-processthreadsapi-startupinfow)| |
|Sharing handles between processes|[DuplicateHandle](https://www.bing.com/search?q=msdn+duplicatehandle&cvid=4e0e222c72434d14b96d6d7bd77c0a21&aqs=edge.0.0j69i57j0j69i64.2180j0j4&FORM=ANAB01&DAF0=1&PC=U531)| |
|Proper usage of Pseudo handles|[GetCurrentProcess](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-getcurrentprocess)| |
|Proper usage of Pseudo handles|[GetCurrentThread](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-getcurrentthread)| |
|Proper usage of Pseudo handles|[DuplicateHandle](https://www.bing.com/search?q=msdn+duplicatehandle&cvid=4e0e222c72434d14b96d6d7bd77c0a21&aqs=edge.0.0j69i57j0j69i64.2180j0j4&FORM=ANAB01&DAF0=1&PC=U531)| |
| **3.4.2 Processes** | | |
|Creating processes|[CreateProcess](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createprocessw)| |
|Creating a process using a token for a user other than the current user|[CreateProcessAsUser](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createprocessasuserw)| |
|Creating processes while passing standard handles|[CreateProcess](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createprocessw)| |
|Creating processes while passing standard handles|[STARTUPINFO](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/ns-processthreadsapi-startupinfow)| |
|Management of process handles|[CloseHandle](https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle)| |
| **3.5.2 Threads** | | |
|Creating and managing threads|[CreateThread](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createthread)| |
|Creating and managing threads|[CloseHandle](https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle)| |
|Proper threading when using the CRT|[_beginthread](https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/beginthread-beginthreadex?view=msvc-170)| |
|Proper threading when using the CRT|[_endthread](https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/endthread-endthreadex?view=msvc-170)| |
| **3.6.2 Synchronization Objects** | | |
|Using the wait APIs|[WaitForSingleObject](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-waitforsingleobject)| |
|Using the wait APIs|[WaitForMultipleObjects](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-waitformultipleobjects)| |
|Using the wait APIs|[WSAWaitForMultipleEvents](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsawaitformultipleevents)| |
|Using event objects|[CreateEvent](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-createeventw)| |
|Using event objects|[WSACreateEvent](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsacreateevent)| |
|Using mutex objects|[CreateMutex](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-createmutexw)| |
|Using critical section objects|[InitializeCriticalSection](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-initializecriticalsection)| |
|Using critical section objects|[EnterCriticalSection](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-initializecriticalsection)| |
|Using critical section objects|[LeaveCriticalSection](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-leavecriticalsection)| |
|Using critical section objects|[DeleteCriticalSection](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-deletecriticalsection)| |
| **3.7.2 File and Device I/O**| | |
|Creating new files |[CreateFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilew)| |
|Safely reading and writing files|[ReadFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-readfile)| |
|Safely reading and writing files|[WriteFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-writefile)| |
|Opening windows devices|[CreateFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilew)| |
|Reading and writing devices directly|[DevioceIoControl](https://docs.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-deviceiocontrol)| |
|Reading and writing files using overlapped I/O|[ReadFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-readfile)| |
|Reading and writing files using overlapped I/O|[WriteFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-writefile)| |
|Reading and writing files using overlapped I/O|[GetOverlappedResult](https://docs.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-getoverlappedresult)| |
|Enumerating files and directories using Win32 API|[FindFirstFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-findfirstfilew)| |
|Enumerating files and directories using Win32 API|[FindNextFile](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-findnextfilew)| |
| **3.8.2 Networking** | | |
|Using Winsock2 APIs|[WSAStartup](https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-wsastartup)| |
|Using Winsock2 APIs|[WSASocket](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsasocketw)| |
|Using Winsock2 APIs|[bind](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-bind)| |
|Using Winsock2 APIs|[listen](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-listen)| |
|Using Winsock2 APIs|[WSAAccept](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsaaccept)| |
|Using Winsock2 APIs|[WSAConnect](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsaconnect)| |
|Using Winsock2 APIs|[WSASend](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsasend)| |
|Using Winsock2 APIs|[WSARecv](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsarecv)| |
|Using Winsock2 APIs|[WSASendTo](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsasendto)| |
|Using Winsock2 APIs|[WSARecvFrom](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsarecvfrom)| |
|Using Winsock2 APIs|[closesocket](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-closesocket)| |
|Using Winsock2 APIs|[WSACleanup](https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-wsacleanup)| |
|Using I/O completion ports with sockets|[CreateIoCompletionPort](https://docs.microsoft.com/en-us/windows/win32/fileio/createiocompletionport)| |
|Using I/O completion ports with sockets|[GetQueuedCompletionStatus](https://docs.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-getqueuedcompletionstatus)| |
| **3.9.2 Memory Management** | | |
|Using the Virtual* allocation APIs|[VirtualAlloc](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualalloc)| |
|Using the Virtual* allocation APIs|[VirtualProtect](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualprotect)| |
|Using the Virtual* allocation APIs|[VirtualFree](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualfree)| |
|Using the Heap* allocation APIs|[GetProcessHeap](https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-getprocessheap)| |
|Using the Heap* allocation APIs|[HeapAlloc](https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapalloc)| |
|Using the Heap* allocation APIs|[HeapFree](https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapfree)| |
|Modifying protections of allocated memory|[VirtualProtect](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualprotect)| |
| **3.10.2 Libraries (DLL / Static)** | | |
|Using a DLL with implicit loading|[pragma comment](https://docs.microsoft.com/en-us/cpp/preprocessor/comment-c-cpp?view=msvc-170)| |
|Using a DLL with implicit loading|[Linker options](https://docs.microsoft.com/en-us/cpp/build/reference/linker-options?view=msvc-170)| |
|Using a DLL with explicit loading|[LoadLibrary](https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-loadlibraryw)| |
|Using a DLL with explicit loading|[GetModuleHandle](https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getmodulehandlew)| |
|Using a DLL with explicit loading|[GetProcAddress](https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getprocaddress)| |
|Using static libraries|[Create a static library](https://docs.microsoft.com/en-us/cpp/build/walkthrough-creating-and-using-a-static-library-cpp?view=msvc-170#CreateLibProject)| |
|Using static libraries|[pragma comment](https://docs.microsoft.com/en-us/cpp/preprocessor/comment-c-cpp?view=msvc-170)| |
|Using static libraries|[Linker options](https://docs.microsoft.com/en-us/cpp/build/reference/linker-options?view=msvc-170)| |
|Simple DLL injection|[OpenProcess](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-openprocess)| |
|Simple DLL injection|[WriteProcessMemory](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-writeprocessmemory)| |
|Simple DLL injection|[CreateRemoteThread](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createremotethread)| |
|In-memory loading DLLs|[VirtualAlloc](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualalloc)| |
|In-memory loading DLLs|[VirtualProtect](https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualprotect)| |
| **3.11.2 Security** | | |
|Enabling and disabling token privileges|[AdjustTokenPrivileges](https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-adjusttokenprivileges)| |
|Creating and using impersonation tokens|[DuplicateToken](https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-duplicatetoken)| |
|Creating and using impersonation tokens|[ImpersonateLoggedOnUser](https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-impersonateloggedonuser)| |
|Creating and using impersonation tokens|[SetThreadToken](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-setthreadtoken)| |
| **3.12.2 Registry** | | |
|Creating registry keys and values|[RegCreateKeyEx](https://docs.microsoft.com/en-us/windows/win32/api/winreg/nf-winreg-regcreatekeyexw)| |
|Creating registry keys and values|[RegSetValueEx](https://docs.microsoft.com/en-us/windows/win32/api/winreg/nf-winreg-regsetvalueexw)| |
|Creating registry keys and values|[RegSetKeyValue](https://docs.microsoft.com/en-us/windows/win32/api/winreg/nf-winreg-regsetkeyvaluew)| |
|Enumerating registry keys and values|[RegEnumKeyEx](https://docs.microsoft.com/en-us/windows/win32/api/winreg/nf-winreg-regenumkeyexw)| |
|Enumerating registry keys and values|[RegEnumValue](https://docs.microsoft.com/en-us/windows/win32/api/winreg/nf-winreg-regenumvaluew)| |
|Persisting a program using a run key|[Run and RunOnce registry keys](https://docs.microsoft.com/en-us/windows/win32/setupapi/run-and-runonce-registry-keys)| |
|Storing binary data in the registry|[RegSetKeyValue](https://docs.microsoft.com/en-us/windows/win32/api/winreg/nf-winreg-regsetkeyvaluew)| |
| **3.13.2 Interprocess Communication (IPC) Mechanisms** | | |
|Using named pipes to communicate between processes|[CreateNamedPipe](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-createnamedpipea)| |
| **3.14.2 Services** |
|Writing a simple windows service application|[Service program tasks](https://docs.microsoft.com/en-us/windows/win32/services/service-program-tasks)| |
|Managing service status|[Handler function](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nc-winsvc-lphandler_function)| |
|Managing service status|[SetServiceStatus](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-setservicestatus)| |
|Creating and controlling services using Win32 API|[OpenSCManager](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-openscmanagerw)| |
|Creating and controlling services using Win32 API|[OpenService](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-openservicew)| |
|Creating and controlling services using Win32 API|[CreateService](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-createservicew)| |
|Creating and controlling services using Win32 API|[RegisterServiceCtrlHandler](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-registerservicectrlhandlerw)| |
|Creating and controlling services using Win32 API|[StartServiceCtrlDispatcher](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-startservicectrldispatcherw)| |
|Creating and controlling services using Win32 API|[CloseServiceHandle](https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-closeservicehandle)| |
| **3.15.2 Component Object Model (COM)** | | |
|Properly initializing and cleaning up COM objects|[CoInitializeEx](https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-coinitializeex)| |
|Properly initializing and cleaning up COM objects|[CoUninitialize](https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-couninitialize)| |
|Using simple COM interfaces|[CoCreateGuid](https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-cocreateguid)| |
|Using simple COM interfaces|[ICLRRuntimeInfo](https://docs.microsoft.com/en-us/dotnet/framework/unmanaged-api/hosting/iclrruntimeinfo-interface)| |
|Using simple COM interfaces|[IXMLDOMDocument](https://docs.microsoft.com/en-us/previous-versions/windows/desktop/ms760218(v=vs.85))| |
| **3.16.2 Crypto Next Generation (CNG) / Bcrypt** | | |
|Encrypt and decrypt data using a symmetric algorithm using CNG|[BCryptEncrypt](https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptencrypt)| |
|Encrypt and decrypt data using a symmetric algorithm using CNG|[BCryptDecrypt](https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptdecrypt)| |
|Encrypt and decrypt data using an asymmetric algorithm using CNG|[BCryptEncrypt](https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptencrypt)| |
|Encrypt and decrypt data using an asymmetric algorithm using CNG|[BCryptDecrypt](https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptdecrypt)| |
|Hash data using CNG|[BCryptHash](https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcrypthash)| |
|Generate random data using CNG|[BCryptGenRandom](https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptgenrandom)| |
| **3.17.2 .NET and .NET Framework** | | |
|Writing a simple .NET console application|[Create a C# console app](https://docs.microsoft.com/en-us/visualstudio/get-started/csharp/tutorial-console?view=vs-2022)| |
|In-memory loading a .NET assembly from a .NET application|[Assembly.Load](https://docs.microsoft.com/en-us/dotnet/api/system.reflection.assembly.load?view=net-6.0)| |
| **3.18.2 Debugging** | | |
|Setting and using a Windows debugger|[WindBag](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg)| |
|Sending debug output to the debugger / system debugger|[OutputDebugString](https://docs.microsoft.com/en-us/windows/win32/api/debugapi/nf-debugapi-outputdebugstringa)| |
